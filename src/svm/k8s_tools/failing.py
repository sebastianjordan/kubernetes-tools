import time


class Action(object):
    def __init__(self, procedure, name):
        self.procedure = procedure
        self.name = name

    def __call__(self):
        return self.procedure()

    def get_name(self):
        return self.name


def retry_with_backup(action, timeout):
    """action must be of type Action"""
    starttime = time.time()
    iterations = 0
    remaining_time = time.time() - starttime
    while True:
        remaining_time = time.time() - starttime
        maybe_result = action()
        if maybe_result is not None:
            return maybe_result
        elif remaining_time > timeout:
            break
        else:
            sleeptime = min(timeout - remaining_time, 2 ** iterations)
            print(
                ('Could not successfully perform `{action}`, retry in '
                 '{sleeptime} seconds'
                ).format(
                    sleeptime=sleeptime,
                    action=action.get_name(),
                )
            )
            time.sleep(sleeptime)
            remaining_time = time.time() - starttime
            iterations += 1
    raise Exception(
        'Timeout while trying to perform `{action}`'.format(
            action=action.get_name(),
        )
    )
