import copy
from pathlib import Path

import pykube


def get_home():
    return str(Path.home())


def get_client():
    return pykube.HTTPClient(
        CustomKubeConfig.from_file(
            get_home() + "/.kube/config",
        )
    )


class CustomKubeConfig(pykube.KubeConfig):
    def __init__(self, doc):
        super().__init__(doc)
        if not self.current_context:
            raise Exception("Cannot create client without current context")

    def get_current_cluster(self):
        current_cluster_name = self.contexts[
            self.current_context
        ]['cluster']
        cluster_config = None
        for cluster in self.doc['clusters']:
            if cluster['name'] == current_cluster_name:
                cluster_config = copy.deepcopy(cluster['cluster'])
        if cluster_config is None:
            raise Exception(
                'kubeconfig does not cluster name set by current context'
            )
        if 'server' not in cluster_config:
            cluster_config['server'] = 'http://localhost'
        pykube.config.BytesOrFile.maybe_set(
            cluster_config,
            'certificate-authority'
        )
        return cluster_config

    @property
    def cluster(self):
        return self.get_current_cluster()

    def get_current_user(self):
        current_user_name = self.contexts[
            self.current_context
        ]['user']
        current_user = None
        for user in self.doc['users']:
            if user['name'] == current_user_name:
                current_user = copy.deepcopy(user)
        pykube.config.BytesOrFile.maybe_set(
            current_user['user'],
            'client-certificate'
        )
        pykube.config.BytesOrFile.maybe_set(
            current_user['user'],
            'client-key'
        )
        return current_user

    @property
    def user(self):
        return self.get_current_user().get('user',{})
