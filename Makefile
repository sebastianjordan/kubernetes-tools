update-nix-deps:
	cd nix/ && pypi2nix \
		--default-overrides \
		-r ../requirements.txt \
		-V 3
