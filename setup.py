from distutils.core import setup

def read_requirements(file_):
    def starts_with_one(txt, xs):
        for x in xs:
            if txt.startswith(x):
                return True
        return False
    lines = []
    with open(file_) as f:
        for line in f.readlines():
            line = line.strip()
            if starts_with_one(
                    line,
                    [ '-e ', 'http://', 'https://', 'git+', 'hg+']
            ):
                lines.append(line.split('#')[1].split('egg=')[1])
            elif line.startswith('#') or line.startswith('-'):
                pass
            else:
                lines.append(line)
    return lines

setup(name='svm-kubernetes-tools',
      version='0.1',
      description='Command line utility for kubernetes',
      author='Sebastian Jordan',
      author_email='jordan@schneevonmorgen.com',
      url='https://bitbucket.org/sebastianjordan/kubernetes-tools',
      packages=['svm.k8s_tools'],
      package_dir = {'': 'src'},
      scripts=['scripts/k8s-query-logs'],
      install_requires = read_requirements('requirements.txt'),
     )
